cmake_minimum_required(VERSION 3.15)

include("cmake/Hunter/HunterGate.cmake")
HunterGate(
    URL "https://github.com/cpp-pm/hunter/archive/v0.25.5.tar.gz"
    SHA1 "a20151e4c0740ee7d0f9994476856d813cdead29"
)

project(asio-hunter)
find_package(Threads REQUIRED)

hunter_add_package(asio)
find_package(asio CONFIG REQUIRED)

add_executable(foo foo.cpp)
target_link_libraries(foo asio::asio_static)
